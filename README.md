# Goals

```mermaid
flowchart LR
    Top([Help Everyone to \n Live More Meaningful Lives])
    
    1([Create New Experiences \n `audio/visual/active/etc`])
        Top === 1
        1 -.- 1a
        1 -.- 1b
        1 -.- 1c
        1a[Music and Composition]
        1b[Games and Stories]
        1c[Inventions/Businesses]

    
    2([Improve Learning and Knowledge])
        Top === 2
        2 -.- 2a
        2 -.- 2b
        2 -.- 2c
        2a["Human-backed Learning Company"]
        2b["Better school system/teaching methods"]
        2c["Better training for work"]

    
    3([Improve work culture])
        Top === 3
        3 -.- 3a
        3 -.- 3b
        3 -.- 3c
        3a["Truly flexible working days/hours"]
        3b["Gallant Sun alliance of small businesses"]
        3c["tbd"]

    
    4([Increase the # of hours \n spent outside and in nature])
        Top === 4
        4 -.- 4a
        4 -.- 4b
        4 -.- 4c
        4 -.- 4d
        4a["Hot Cold Clothing"]
        4b["Phone Games/AR outdoors"]
        4c["Lower the cost/make knowing about outdoor experience easier"]
        4d["Improve parks/places"]

    5([Help The US/World create a \n culture of truth, tolerance, and understanding])
        Top === 5
        5 -.- 5a
        5 -.- 5b
        5 -.- 5c
        5a["Become a politician or help there"]
        5b["Improve Rich / poor understandings of each other"]
        5c["Expose everyone to more stories from the 'other' groups"]

    6([Improve Human Health via personal practices])
        Top === 6
        6 -.- 6a
        6 -.- 6b
        6 -.- 6c
        6 -.- 6d
        6a["Improve knowledge of what is healthy food"]
        6b["Reduce addictions"]
        6c["Increase slept hours"]
        6d["Tackle High Health Care costs in US"]
```
